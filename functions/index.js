"use strict";

const admin = require("firebase-admin");
const functions = require("firebase-functions");
const { StatusCodes } = require("http-status-codes");

// Moments library to format dates.
const moment = require("moment");
const config = require("./src/config");
// CORS Express middleware to enable CORS Requests.
const cors = require("cors")({
  origin: true,
});

// Initialize Functions
admin.initializeApp();

const { firebase_crash } = require("./src/crash");
const crash_report = require("./src/report/crash_report");
const upload_image = require("./src/upload/upload_image");

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions

// exports.reportSentry = functions.https.onRequest((req, res) => {

// },

exports.reportCrash = functions.https.onRequest((req, res) => {
  if (req.method !== "POST") {
    return res.status(StatusCodes.FORBIDDEN).send("Forbidden!");
  }

  return cors(req, res, () => {
    //const params = req.query;
    const { content = "", base64 = "" } = req.body || {};

    if (!content) {
      res
        .status(StatusCodes.NOT_FOUND)
        .send("Report Crash failed with content is null!");
      return;
    }

    if (!base64) {
      res
        .status(StatusCodes.NOT_FOUND)
        .send("Report Crash failed with base64 is null!");
      return;
    }

    upload_image
      .uploadImage(base64)
      .then((imageUrl) => {
        // notify to general cell
        // crash_report.notifyHangouts(config.ROOM_URL.CELL_GENERAL, content, imageUrl)

        // notify to another cell
        const appId = crash_report.getAppId(content);
        if (!appId) {
          res
            .status(StatusCodes.NOT_FOUND)
            .send(
              "Report Crash general success.\nReport Crash failed with appId not found!"
            );
          return;
        }
        const url = crash_report.getHttpUrl(appId);

        crash_report.notifyHangouts(url, content, imageUrl);
        return;
      })
      .catch((err) => {
        console.log("uploadImage failed: ", err);
      });

    res.status(StatusCodes.OK).send("Report Crash success!");
  });
});
