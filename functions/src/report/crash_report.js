const config = require("../config");
const requestPromise = require("request-promise-native");
const { app } = require("firebase-admin");

// MoMo Bot with webhook

// Mapping
// =====================
// Group team
// Cell team
// Content of crash
// Image of crash
// UserId (Team) / All
// =====================
function getAppId(message) {
  let cloneMessage = message;
  let appId = null;
  try {
    const indexAppId = cloneMessage.indexOf("AppId:");
    if (indexAppId >= 0) {
      const indexName = cloneMessage.indexOf("Name:");
      appId = cloneMessage.substring(indexAppId + 6, indexName).trim();
    }
  } catch (error) {
    console.log("getAppId error: ", error);
  }

  return appId;
}

function getHttpUrl(appId) {
  let httpURL = config.ROOM_URL.CELL_GENERAL;
  for (let key in config.APP_MAPPING) {
    if (config.APP_MAPPING[key].includes(appId)) {
      httpURL = config.ROOM_URL[key];
    }
  }

  return httpURL;
}

// Helper function that posts to Hangouts MoMo Platform about the new issue
async function notifyHangouts(url, message, imageUrl = '') {
  let contentMessage = '<users/all>\n'
  if (imageUrl !== '' && imageUrl !== undefined && imageUrl !== null) contentMessage += `ImageUrl: ${imageUrl}\n`
  contentMessage += message

  return requestPromise(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    json: true,
    body: {
      text: contentMessage,
    },
  });
}

module.exports = {
  notifyHangouts,
  getAppId,
  getHttpUrl,
};
