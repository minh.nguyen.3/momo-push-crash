module.exports = {
  ROOM_URL: {
    CELL_GENERAL:
      "https://chat.googleapis.com/v1/spaces/AAAAUC2Bm2c/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=XTjhjadmbJfpUfRtCpfsYjgWBF70ylsUKYpTcDu50a0%3D&threadKey=nUQ_JIhNyYQ",
    CELL_1:
      "https://chat.googleapis.com/v1/spaces/AAAAZATUbvM/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=Pcv-BmwS3D6FDHPfGXldvFLrMg9bxeTxThF39r41AeU%3D&threadKey=plprGBeGBDA",
    CELL_ASON:
      "https://chat.googleapis.com/v1/spaces/AAAAoFZDONI/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=U4363gS03cSeKEKZyQObY2rxoHxjdfckye8l9zOQHQo%3D&threadKey=ls4E1yNNwV4",
    CELL_ACUONG:
      "https://chat.googleapis.com/v1/spaces/AAAAtxg4364/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=_Li8smCoXTLFznl0-VZNZ4bwEwoieDKRFT0_JIvSas8%3D&threadKey=rbpy2CLOY6g",
    CELL_3:
      "https://chat.googleapis.com/v1/spaces/AAAAWk8H1fo/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=Yww-gLEgr_QESqkOYux8WeBtxQcTzyFAtCOwXXIRL4Q%3D", // no threadKey
    CELL_4:
      "https://chat.googleapis.com/v1/spaces/AAAA8K2MANA/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=BGvEIdQQph1Bxy4p6eNpLky8LMPhe6oORPEW1Te5gSQ%3D&threadKey=MVWaLedp_h4",
    CELL_BOOKING:
      "https://chat.googleapis.com/v1/spaces/AAAAJcP6bAM/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=e9OiiW9JAOjogq0MSmilgCCL7I6Ron5o84oHxij1TII%3D&threadKey=rIrvSMiKh-Y",
    CELL_FINANCE:
      "https://chat.googleapis.com/v1/spaces/AAAANMmp9Uk/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=fQS8AjelZ4Ub1pihGOBrOF6SXFElDD5sh_OGycum9Wg%3D&threadKey=smUKSxSXVm8",
    CELL_GAME:
      "https://chat.googleapis.com/v1/spaces/AAAAassgSjM/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=RUnJ_F4ptuAM7nXCUsRSdmtSXwQdbI27qlVS4os94Rk%3D&threadKey=w0tAAvAWwHA",
    CELL_CHAT:
      "https://chat.googleapis.com/v1/spaces/AAAAjz4zKQI/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=T8kpD4ZikjvDfIxpI8VCR51wbF8PlK3lQE2AZU9fvms%3D", // no threadKey
    CELL_HELIOS:
      "https://chat.googleapis.com/v1/spaces/AAAAy5por_Y/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=OD40TkMYzhYpeVAAs8bU865-zde4_wi-sbvsoBELnRk%3D&threadKey=ArI-RUqlgXU",
  },

  APP_MAPPING: {
    CELL_GENERAL: [],
    CELL_1: [
      "vn.momo.driverdeposit",
      "vn.momo.billpay",
      "vn.momo.mobilecenter",
      "vn.momo.hoa18",
    ],
    CELL_ASON: [
      "vn.momo.moneypool",
      "vn.momo.receivemoneylink",
      "vn.momo.greetingcard",
      "vn.momo.requestmoney",
    ],
    CELL_ACUONG: ["vn.momo.cornerstone", "vn.momo.promotion"],
    CELL_3: [
      "vn.momo.oauthorize",
      "vn.momo.guidelinkandpayment",
      "vn.momo.authenlinkingservices",
      "vn.momo.highlandcoffee",
      "vn.momo.thecoffeehouse",
      "vn.momo.onlinestore",
      "vn.momo.webapp",
    ],
    CELL_4: ["vn.momo.platform"],
    CELL_BOOKING: [
      "vn.momo.booking_train",
      "vn.momo.booking_bus",
      "vn.momo.vetauhoa",
      "vn.momo.vexere",
      "vn.momo.airline",
      "vn.momo.cinema",
    ],
    CELL_FINANCE: [
      "vn.momo.utilities",
      "vn.momo.fiweb",
      "vn.momo.prudential_agent",
      "vn.momo.dynamicform",
      "vn.momo.investment",
      "vn.momo.loan_fastmoney",
      "vn.momo.my_insurance_policy",
      "vn.momo.my_paylater",
    ],
    CELL_GAME: [
      "vn.momo.cashback",
      "vn.momo.walking",
      "vn.momo.eatforlife",
      "vn.momo.lixi2021",
    ],
    CELL_CHAT: ["vn.momo.chat"],
    CELL_HELIOS: [
      "vn.momo.ekyc",
      "vn.momo.expense",
      "vn.momo.passbook",
      "vn.momo.gotitecommerce",
      "vn.momo.transactionhistorynew",
      "vn.momo.transactionhistory",
    ],
  },
};
