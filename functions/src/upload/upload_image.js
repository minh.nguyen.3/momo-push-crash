"use strict";

const admin = require("firebase-admin");
const { v4: uuidv4 } = require("uuid");
const stream = require("stream");

const storage = admin.storage();
const IMAGE_EXPIRE = 30; //30 days
const IMAGE_DESTINATION = "crashes";
const IMAGE_CONTENT_TYPE = "image/png";

function checkImageBase64(base64) {
  if (base64.startsWith("data:image/png;base64,")) {
      let indexBase64 = base64.indexOf('data:image/png;base64,');
      return base64.substring(indexBase64);
  } else {
    return base64;
  }
}

function getImageName(uuid, ext = ".png") {
  if (!uuid) return "";
  let tmpUUID = "";
  let uuidArray = uuid.split("-");
  uuidArray.forEach((element) => {
    tmpUUID += element.charAt(0);
  });

  return `_image_${tmpUUID}${ext}`;
}

function uploadImage(base64) {
  return new Promise((resolve, reject) => {
    const bucket = storage.bucket();
    const validBase64 = checkImageBase64(base64);
    const image_name = getImageName(uuidv4());
    const fileDestination = `${IMAGE_DESTINATION}/${image_name}`;

    // initialize with create buffer stream
    let bufferStream = new stream.PassThrough();
    bufferStream.end(Buffer.from(validBase64, "base64"));
    // Create a reference to the new image file
    const file = bucket.file(fileDestination);

    bufferStream
      .pipe(
        file.createWriteStream({
          metadata: {
            contentType: IMAGE_CONTENT_TYPE,
          },
          public: true,
          contentType: IMAGE_CONTENT_TYPE,
        })
      )
      .on("error", (error) => {
        reject(new Error(`uploadImage - Error while uploading image: ${JSON.stringify(error)}`));
      })
      .on("finish", () => {
        let date = new Date();
        file
          .getSignedUrl({
            action: "read",
            expires: date.setDate(date.getDate() + IMAGE_EXPIRE),
          })
          .then((fileUrls) => {
            resolve(fileUrls[0]);
            return;
          })
          .catch(err => {
            reject(new Error(`file.getSignedUrl failed: ${JSON.stringify(err)}`));
          })
      });
  });
}

module.exports = {
  uploadImage,
};
