const functions = require("firebase-functions");
const crash_report = require('../report/crash_report');

// [START on_new_issue]
exports.postOnNewIssue = functions.crashlytics.issue().onNew(async (issue) => {
    const issueId = issue.issueId;
    const issueTitle = issue.issueTitle;
    const appName = issue.appInfo.appName;
    const appPlatform = issue.appInfo.appPlatform;
    const latestAppVersion = issue.appInfo.latestAppVersion;
  
    const message =
      `<!here|here> There is a new issue - ${issueTitle} (${issueId}) ` +
      `in ${appName}, version ${latestAppVersion} on ${appPlatform}`;
  
    await crash_report.notifyHangouts(message);
    console.log(`Posted new issue ${issueId} successfully to Hangouts`);
  });
  // [END on_new_issue]
  
  exports.postOnRegressedIssue = functions.crashlytics
    .issue()
    .onRegressed(async (issue) => {
      const issueId = issue.issueId;
      const issueTitle = issue.issueTitle;
      const appName = issue.appInfo.appName;
      const appPlatform = issue.appInfo.appPlatform;
      const latestAppVersion = issue.appInfo.latestAppVersion;
      const resolvedTime = issue.resolvedTime;
  
      const message =
        `<!here|here> There is a regressed issue ${issueTitle} (${issueId}) ` +
        `in ${appName}, version ${latestAppVersion} on ${appPlatform}. This issue was previously ` +
        `resolved at ${new Date(resolvedTime).toString()}`;
  
      await crash_report.notifyHangouts(message);
      console.log(`Posted regressed issue ${issueId} successfully to Hangouts`);
    });
  
  exports.postOnVelocityAlert = functions.crashlytics
    .issue()
    .onVelocityAlert(async (issue) => {
      const issueId = issue.issueId;
      const issueTitle = issue.issueTitle;
      const appName = issue.appInfo.appName;
      const appPlatform = issue.appInfo.appPlatform;
      const latestAppVersion = issue.appInfo.latestAppVersion;
      const crashPercentage = issue.velocityAlert.crashPercentage;
  
      const message =
        `<!here|here> There is an issue ${issueTitle} (${issueId}) ` +
        `in ${appName}, version ${latestAppVersion} on ${appPlatform} that is causing ` +
        `${crashPercentage.toFixed(2)}% of all sessions to crash.`;
  
      await crash_report.notifyHangouts(message);
      console.log(`Posted velocity alert ${issueId} successfully to Hangouts`);
    });
  